import {ReactNode} from 'react'
import Header from '../header'
import Footer from '../footer'
import styles from './layout.module.css'
import Head from 'next/head'

interface LayoutProps {
    children: ReactNode;
    pageTitle: string;
}
const layout = (props: LayoutProps) => {
    const {children, pageTitle} = props;
    return(
        <>
        <Head>
        <title>NextJs Basic | {pageTitle}</title>
        <meta name="description" content="Website NextJs Basic" />
        </Head>
        <div className={styles.container}>
            <Header />
                <div className={styles.content}>
                    {children}
                </div>
            <Footer />
        </div>
        </>
    )
}

export default layout