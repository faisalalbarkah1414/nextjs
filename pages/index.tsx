import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Layout from '../components/layout'

const Home: NextPage = () => {
  return (
      <Layout pageTitle="Home Page">
        <Image src="/kuroko.jpg" width={200} height={200} alt="profile" />
        <h1 className={styles['title-homepage']}>Welcome NextJs</h1>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa quos, accusamus laudantium ea consectetur ratione eaque sit quam qui placeat, maxime cum, quo iusto! Fuga possimus dolorum ut debitis aut.</p>
      </Layout>
  )
}

export default Home
